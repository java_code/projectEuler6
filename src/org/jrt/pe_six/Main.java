/**
 * org.jrt.pe_six is a list of operations to complete project euler problem six
 */
package org.jrt.pe_six;

/**
 * The sum of the squares of the first ten natural numbers is,
 * 1^2 + 2^2 + ... + 10^2 = 385
 * 
 * The square of the sum of the first ten natural numbers is,
 * (1 + 2 + ... + 10)^2 = 552 = 3025
 * 
 * Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.
 * 
 * Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
 * 
 * @author jrtobac
 */
public class Main {

	public static void main(String[] args) {
		double sumOfSquares = 0;
		double squareOfSums = 0;
		double difference = 0;
		
		for(double i = 1; i <= 100; i++) {
			sumOfSquares += Math.pow(i, 2);
			squareOfSums += i;
		}
		
		squareOfSums = Math.pow(squareOfSums, 2);
		
		difference = squareOfSums - sumOfSquares;
		
		System.out.printf("%.0f\n", difference);
	}
}
